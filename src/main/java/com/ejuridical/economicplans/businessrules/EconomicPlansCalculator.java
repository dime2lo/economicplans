package com.ejuridical.economicplans.businessrules;

import com.ejuridical.economicplans.dtos.EconomicPlansCalculatedValuesResponse;
import com.ejuridical.economicplans.dtos.EconomicPlansRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EconomicPlansCalculator {
    private final BigDecimal COLLOR_ONE_MAY_IPC_IBGE_VARIATION = new BigDecimal(0.0787);
    private final BigDecimal COLLOR_ONE_APRIL_IPC_IBGE_VARIATION = new BigDecimal(0.448);
    private final BigDecimal BRESSER_IPC_IBGE_VARIATION = new BigDecimal(0.2606);
    private final BigDecimal SUMMER_IPC_IBGE_VARIATION = new BigDecimal(0.4272);


    private final BigDecimal COLLOR_ONE_MAY_RESTATEMENT_INDEX = new BigDecimal(0.1957830);
    private final BigDecimal COLLOR_ONE_APRIL_RESTATEMENT_INDEX = new BigDecimal(0.2073478);
    private final BigDecimal BRESSER_RESTATEMENT_INDEX = new BigDecimal(0.3974311);
    private final BigDecimal SUMMER_RESTATEMENT_INDEX = new BigDecimal(16.2319260);

    private final BigDecimal CONTRACTUAL_REMUNERATORY_INDEX = new BigDecimal(0.005);

    private final String COLLOR_ONE_MAY = "COLLOR_ONE_MAY";
    private final String COLLOR_ONE_APRIL = "COLLOR_ONE_APRIL";
    private final String BRESSER = "BRESSER";
    private final String VERAO = "VERAO";

    private final String IPC_01_1989_VERAO = "IPC_01_1989_VERAO";
    private final String IPC_05_1990_COLLOR_1 = "IPC_05_1990_COLLOR_1";
    private final String IPC_02_1991_COLLOR_2 = "IPC_02_1991_COLLOR_2";
    private final String IPC_04_1990_COLLOR_1 = "IPC_04_1990_COLLOR_1";

    private final BigDecimal IPC_01_1989_VERAO_INFLATIONARY_PURGE_INDEX = new BigDecimal(0.1664);
    private final BigDecimal IPC_05_1990_COLLOR_1_INFLATIONARY_PURGE_INDEX = new BigDecimal(0.0236);
    private final BigDecimal IPC_02_1991_COLLOR_2_INFLATIONARY_PURGE_INDEX = new BigDecimal(0.1389);
    private final BigDecimal IPC_04_1990_COLLOR_1_INFLATIONARY_PURGE_INDEX = new BigDecimal(0.4480);

    private final String SIX_PERCENT_YEARLY_INTEREST_ON_ARREARS_OPTION = "6";
    private final String TWELVE_PERCENT_YEARLY_INTEREST_ON_ARREARS_OPTION = "12";

    private final BigDecimal SIX_PERCENT_YEARLY_INTEREST_ON_ARREARS_MONTHLY_INDEX = new BigDecimal(0.005);
    private final BigDecimal TWELVE_PERCENT_YEARLY_INTEREST_ON_ARREARS_MONTHLY_INDEX = new BigDecimal(0.01);

    public EconomicPlansCalculatedValuesResponse calculate(EconomicPlansRequest request) {
        EconomicPlansCalculatedValuesResponse economicPlansCalculatedValuesResponse = new EconomicPlansCalculatedValuesResponse();

        BigDecimal totalPurgesToConsider = calculateIpcIbgeVariation(request.economicPlan);
        BigDecimal contractualRemuneratoryInterest = calculateContractualRemuneratoryInterest();

        // TODO Pegar Índice do banco de dados
        BigDecimal restatementIndex = calculateRestatementIndex(request.economicPlan, request.purges);
        BigDecimal interestPercentage = calculateInterestPercentage(request.interestOnArrearsInitialDate, request.interestOnArrearsEndDate, request.yearlyInterestOnArrears);

        BigDecimal dueMonetaryCorrection = calculateDueMonetaryCorrection(request.baseBalance, totalPurgesToConsider);
        BigDecimal monetaryCorrectionDifference = calculateMonetaryCorrectionDifference(dueMonetaryCorrection, request.creditedMonetaryCorrection);
        BigDecimal dueInterest = calculateDueInterest(request.baseBalance, dueMonetaryCorrection, contractualRemuneratoryInterest);
        BigDecimal interestDifference = calculateInterestDifference(dueInterest, request.creditedInterest);
        BigDecimal totalDifferences = calculateTotalDifferences(monetaryCorrectionDifference, interestDifference);

        BigDecimal currentCurrencyCorrectedValue = calculateCurrentCurrencyCorrectedValue(totalDifferences, restatementIndex);
        BigDecimal interestValueInCurrentCurrency = calculateInterestValueInCurrentCurrency(currentCurrencyCorrectedValue, interestPercentage);

        BigDecimal totalValueRestated = calculateTotalValueRestated(currentCurrencyCorrectedValue, interestValueInCurrentCurrency);
        BigDecimal feesOfCounsel = calculateFeesOfCounsel(totalValueRestated, request.feesOfCounselPercentage);
        BigDecimal generalTotal = calculateGeneralTotal(totalValueRestated, feesOfCounsel, request.courtCosts);

        economicPlansCalculatedValuesResponse.totalPurgesToConsider = totalPurgesToConsider;
        economicPlansCalculatedValuesResponse.contractualRemuneratoryInterest = contractualRemuneratoryInterest;
        economicPlansCalculatedValuesResponse.restatementIndex = restatementIndex;
        economicPlansCalculatedValuesResponse.interestPercentage = interestPercentage;
        economicPlansCalculatedValuesResponse.dueMonetaryCorrection = dueMonetaryCorrection;
        economicPlansCalculatedValuesResponse.monetaryCorrectionDifference = monetaryCorrectionDifference;
        economicPlansCalculatedValuesResponse.dueInterest = dueInterest;
        economicPlansCalculatedValuesResponse.interestDifference = interestDifference;
        economicPlansCalculatedValuesResponse.totalDifferences = totalDifferences;
        economicPlansCalculatedValuesResponse.currentCurrencyCorrectedValue = currentCurrencyCorrectedValue;
        economicPlansCalculatedValuesResponse.interestValueInCurrentCurrency = interestValueInCurrentCurrency;
        economicPlansCalculatedValuesResponse.totalValueRestated = totalValueRestated;
        economicPlansCalculatedValuesResponse.feesOfCounsel = feesOfCounsel;
        economicPlansCalculatedValuesResponse.generalTotal = generalTotal;

        return economicPlansCalculatedValuesResponse;
    }

    public BigDecimal calculateIpcIbgeVariation(String economicPlan) {
        if (economicPlan.equals(COLLOR_ONE_MAY))
            return COLLOR_ONE_MAY_IPC_IBGE_VARIATION;
        if (economicPlan.equals(COLLOR_ONE_APRIL))
            return COLLOR_ONE_APRIL_IPC_IBGE_VARIATION;
        if (economicPlan.equals(BRESSER))
            return BRESSER_IPC_IBGE_VARIATION;
        if (economicPlan.equals(VERAO))
            return SUMMER_IPC_IBGE_VARIATION;
        return BigDecimal.ZERO;
    }

    public BigDecimal calculateContractualRemuneratoryInterest() {
        return CONTRACTUAL_REMUNERATORY_INDEX;
    }

    // TODO CRIAR TESTES
    public BigDecimal calculateRestatementIndex(String economicPlan, ArrayList<String> purges) {
        BigDecimal restatementIndex = getCurrentEconomicPlanRestatementIndex(economicPlan);

        for (String purgeName : purges) {
            BigDecimal purgeIndex = getInflationaryPurgeIndex(purgeName);
            restatementIndex = restatementIndex.add(restatementIndex.multiply(purgeIndex));
        }
        return restatementIndex;
    }

    public BigDecimal getCurrentEconomicPlanRestatementIndex(String economicPlan) {
        if (economicPlan.equals(COLLOR_ONE_MAY))
            return COLLOR_ONE_MAY_RESTATEMENT_INDEX;
        if (economicPlan.equals(COLLOR_ONE_APRIL))
            return COLLOR_ONE_APRIL_RESTATEMENT_INDEX;
        if (economicPlan.equals(BRESSER))
            return BRESSER_RESTATEMENT_INDEX;
        if (economicPlan.equals(VERAO))
            return SUMMER_RESTATEMENT_INDEX;
        return BigDecimal.ZERO;
    }
    public BigDecimal getInflationaryPurgeIndex(String purgeName) {
        BigDecimal purgeIndex = BigDecimal.ZERO;
        if (purgeName.equals(IPC_01_1989_VERAO))
            purgeIndex = IPC_01_1989_VERAO_INFLATIONARY_PURGE_INDEX;
        if (purgeName.equals(IPC_04_1990_COLLOR_1))
            purgeIndex = IPC_04_1990_COLLOR_1_INFLATIONARY_PURGE_INDEX;
        if (purgeName.equals(IPC_05_1990_COLLOR_1))
            purgeIndex = IPC_05_1990_COLLOR_1_INFLATIONARY_PURGE_INDEX;
        if (purgeName.equals(IPC_02_1991_COLLOR_2))
            purgeIndex = IPC_02_1991_COLLOR_2_INFLATIONARY_PURGE_INDEX;
        return purgeIndex;
    }

    public BigDecimal calculateInterestPercentage(String interestOnArrearsInitialDate, String interestOnArrearsEndDate, String yearlyInterestOnArrears) {
        BigDecimal interestOnArrears = getInterestOnArrearsMonthlyIndex(yearlyInterestOnArrears);
        Date startDate = new Date();
        Date endDate = new Date();
        try {
            startDate = new SimpleDateFormat("yyyy-MM").parse(interestOnArrearsInitialDate);
            endDate = new SimpleDateFormat("yyyy-MM").parse(interestOnArrearsEndDate);
        } catch (ParseException e) {

        }
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(startDate);
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
        return interestOnArrears.multiply(new BigDecimal(diffMonth));
    }

    public BigDecimal getInterestOnArrearsMonthlyIndex(String yearlyInterestOnArrears) {
        if (yearlyInterestOnArrears.equals(SIX_PERCENT_YEARLY_INTEREST_ON_ARREARS_OPTION))
            return SIX_PERCENT_YEARLY_INTEREST_ON_ARREARS_MONTHLY_INDEX;
        if (yearlyInterestOnArrears.equals(TWELVE_PERCENT_YEARLY_INTEREST_ON_ARREARS_OPTION))
            return TWELVE_PERCENT_YEARLY_INTEREST_ON_ARREARS_MONTHLY_INDEX;
        return BigDecimal.ZERO;
    }

    public BigDecimal calculateDueMonetaryCorrection(BigDecimal baseBalance, BigDecimal totalPurgesToConsider) {
        return (baseBalance.multiply(totalPurgesToConsider)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateMonetaryCorrectionDifference(BigDecimal dueMonetaryCorrection, BigDecimal creditedMonetaryCorrection) {
        return (dueMonetaryCorrection.subtract(creditedMonetaryCorrection)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateDueInterest(BigDecimal baseBalance, BigDecimal dueMonetaryCorrection, BigDecimal contractualRemuneratoryInterest) {
        return ((baseBalance.add(dueMonetaryCorrection)).multiply(contractualRemuneratoryInterest)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateInterestDifference(BigDecimal dueInterest, BigDecimal creditedInterest) {
        return (dueInterest.subtract(creditedInterest)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateTotalDifferences(BigDecimal monetaryCorrectionDifference, BigDecimal interestDifference) {
        return (monetaryCorrectionDifference.add(interestDifference)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateCurrentCurrencyCorrectedValue(BigDecimal totalDifferences, BigDecimal restatementIndex) {
        return (totalDifferences.multiply(restatementIndex)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateInterestValueInCurrentCurrency(BigDecimal currentCurrencyCorrectedValue, BigDecimal interestPercentage) {
        return (currentCurrencyCorrectedValue.multiply(interestPercentage)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateTotalValueRestated(BigDecimal currentCurrencyCorrectedValue, BigDecimal interestValueInCurrentCurrency) {
        return (currentCurrencyCorrectedValue.add(interestValueInCurrentCurrency)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateFeesOfCounsel(BigDecimal totalValueRestated, BigDecimal feesOfCounselPercentage) {
        return (totalValueRestated.multiply(feesOfCounselPercentage)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateGeneralTotal(BigDecimal totalValueRestated, BigDecimal feesOfCounsel, BigDecimal courtCosts) {
        return (totalValueRestated.add(feesOfCounsel).add(courtCosts)).setScale(2, RoundingMode.HALF_UP);
    }
}
