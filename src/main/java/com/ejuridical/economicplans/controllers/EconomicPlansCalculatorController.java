package com.ejuridical.economicplans.controllers;

import com.ejuridical.economicplans.dtos.EconomicPlansCalculatedValuesResponse;
import com.ejuridical.economicplans.dtos.EconomicPlansCalculatorRequest;
import com.ejuridical.economicplans.dtos.EconomicPlansCalculatorResponse;
import com.ejuridical.economicplans.businessrules.EconomicPlansCalculator;
import com.ejuridical.economicplans.dtos.EconomicPlansRequest;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class EconomicPlansCalculatorController {
    EconomicPlansCalculator economicPlansCalculator = new EconomicPlansCalculator();
    @RequestMapping(value = "/leo", method = RequestMethod.GET)
    public  String leo() {
        //System.out.println("Hello World");

        return "hello";
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody EconomicPlansCalculatorResponse calculate(@RequestBody EconomicPlansCalculatorRequest input) {
        EconomicPlansCalculatorResponse calculatorResponse = new EconomicPlansCalculatorResponse();
        // validateRequest();

        EconomicPlansRequest calculatorRequest = new EconomicPlansRequest();
        System.out.println(input.baseBalance);
        calculatorRequest.baseBalance = new BigDecimal(input.baseBalance);
        calculatorRequest.creditedMonetaryCorrection = new BigDecimal(input.creditedMonetaryCorrection);
        calculatorRequest.creditedInterest = new BigDecimal(input.creditedInterest);
        calculatorRequest.feesOfCounselPercentage = new BigDecimal(input.feesOfCounselPercentage);
        calculatorRequest.courtCosts = new BigDecimal(input.courtCosts);
        calculatorRequest.economicPlan = input.economicPlan;
        calculatorRequest.interestOnArrearsInitialDate = input.interestOnArrearsInitialDate;
        calculatorRequest.yearlyInterestOnArrears = input.yearlyInterestOnArrears;
        calculatorRequest.purges = input.typesOfPurgesToConsider;
        DateFormat df = new SimpleDateFormat("yyyy-MM");
        calculatorRequest.interestOnArrearsEndDate = df.format(new Date());

        EconomicPlansCalculatedValuesResponse response = economicPlansCalculator.calculate(calculatorRequest);

        calculatorResponse.economicPlan = input.economicPlan;
        calculatorResponse.initialMonthYear = calculatorRequest.interestOnArrearsInitialDate;
        calculatorResponse.currentMonthYear = calculatorRequest.interestOnArrearsEndDate;

        calculatorResponse.dueMonetaryCorrection = response.dueMonetaryCorrection.toString();
        calculatorResponse.monetaryCorrectionDifference = response.monetaryCorrectionDifference.toString();
        calculatorResponse.dueInterest = response.dueInterest.toString();
        calculatorResponse.interestDifference = response.interestDifference.toString();
        calculatorResponse.totalDifferences = response.totalDifferences.toString();
        calculatorResponse.contractualRemuneratoryInterest = response.contractualRemuneratoryInterest.toString();
        calculatorResponse.totalPurgesToConsider = response.totalPurgesToConsider.toString();

        calculatorResponse.currentCurrencyCorrectedValue = response.currentCurrencyCorrectedValue.toString();
        calculatorResponse.interestValueInCurrentCurrency = response.interestValueInCurrentCurrency.toString();
        calculatorResponse.restatementIndex = response.restatementIndex.toString();
        calculatorResponse.totalValueRestated = response.totalValueRestated.toString();
        calculatorResponse.feesOfCounsel = response.feesOfCounsel.toString();
        calculatorResponse.generalTotal = response.generalTotal.toString();

        calculatorResponse.accountHolder = input.accountHolder;
        calculatorResponse.accountNumber = input.accountNumber;
        calculatorResponse.baseDay = input.baseDay;
        calculatorResponse.baseBalance = input.baseBalance;
        calculatorResponse.creditedMonetaryCorrection = input.creditedMonetaryCorrection;
        calculatorResponse.creditedInterest = input.creditedInterest;
        calculatorResponse.deadlineForRestatement = input.deadlineForRestatement;
        calculatorResponse.interestOnArrearsInitialDate = input.interestOnArrearsInitialDate;
        calculatorResponse.yearlyInterestOnArrears = input.yearlyInterestOnArrears;
        calculatorResponse.feesOfCounselPercentage = input.feesOfCounselPercentage;
        calculatorResponse.courtCosts = input.courtCosts;
        calculatorResponse.monetaryCorrectionCriteria = input.monetaryCorrectionCriteria;
        calculatorResponse.typesOfPurgesToConsider = input.typesOfPurgesToConsider;

        return calculatorResponse;
    }
}
