package com.ejuridical.economicplans.dtos;

import java.util.ArrayList;

public class EconomicPlansCalculatorRequest {
    public String economicPlan;
    public String accountNumber;
    public String accountHolder;
    public String baseDay;
    public String baseBalance;
    public String creditedMonetaryCorrection;
    public String creditedInterest;
    public String deadlineForRestatement;
    public String interestOnArrearsInitialDate;
    public String yearlyInterestOnArrears;
    public String feesOfCounselPercentage;
    public String courtCosts;
    public String monetaryCorrectionCriteria;
    public ArrayList<String> typesOfPurgesToConsider;
}
