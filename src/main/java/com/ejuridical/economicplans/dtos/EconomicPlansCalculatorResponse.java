package com.ejuridical.economicplans.dtos;

import java.util.ArrayList;

public class EconomicPlansCalculatorResponse {
    public String economicPlan;

    public String initialMonthYear;
    public String currentMonthYear;

    public String accountHolder;
    public String accountNumber;

    public String dueMonetaryCorrection;
    public String monetaryCorrectionDifference;
    public String dueInterest;
    public String interestDifference;
    public String totalDifferences;
    public String contractualRemuneratoryInterest;
    public String totalPurgesToConsider;

    public String currentCurrencyCorrectedValue;
    public String interestValueInCurrentCurrency;
    public String restatementIndex;

    public String totalValueRestated;
    public String feesOfCounsel;
    public String generalTotal;

    public String baseDay;
    public String baseBalance;
    public String creditedMonetaryCorrection;
    public String creditedInterest;
    public String deadlineForRestatement;
    public String interestOnArrearsInitialDate;
    public String yearlyInterestOnArrears;
    public String feesOfCounselPercentage;
    public String courtCosts;
    public String monetaryCorrectionCriteria;
    public ArrayList<String> typesOfPurgesToConsider;
}
