package com.ejuridical.economicplans.dtos;

import java.math.BigDecimal;

public class EconomicPlansCalculatedValuesResponse {
    public BigDecimal totalPurgesToConsider;
    public BigDecimal contractualRemuneratoryInterest;
    public BigDecimal restatementIndex;
    public BigDecimal interestPercentage;
    public BigDecimal dueMonetaryCorrection;
    public BigDecimal monetaryCorrectionDifference;
    public BigDecimal dueInterest;
    public BigDecimal interestDifference;
    public BigDecimal totalDifferences;
    public BigDecimal currentCurrencyCorrectedValue;
    public BigDecimal interestValueInCurrentCurrency;
    public BigDecimal totalValueRestated;
    public BigDecimal feesOfCounsel;
    public BigDecimal generalTotal;
}
