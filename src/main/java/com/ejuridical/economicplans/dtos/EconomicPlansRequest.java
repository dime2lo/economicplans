package com.ejuridical.economicplans.dtos;

import java.math.BigDecimal;
import java.util.ArrayList;

public class EconomicPlansRequest {
    public String economicPlan;
    public BigDecimal baseBalance;
    public BigDecimal creditedMonetaryCorrection;
    public BigDecimal creditedInterest;
    public BigDecimal feesOfCounselPercentage;
    public BigDecimal courtCosts;
    public String interestOnArrearsEndDate;
    public String interestOnArrearsInitialDate;
    public String yearlyInterestOnArrears;
    public ArrayList<String> purges;
}
