<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>e-PlanosEcon&ocirc;micos</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="https://unpkg.com/vue"></script>
</head>

<body>
<div id="app">
    <section class="section">
        <div class="container">
            <app-calculator></app-calculator>
        </div>
    </section>
</div>
<script>
    Vue.component('app-calculator', {
        template: `
            <div>
                <div class="tabs is-boxed">
                    <ul>
                        <li :class="{'is-active': isActive('bresser')}" @click="changeCalculator('bresser')"><a>Bresser (06/1987)</a></li>
                        <li :class="{'is-active': isActive('verao')}" @click="changeCalculator('verao')"><a>Ver&atilde;o (01/1989)</a></li>
                        <li :class="{'is-active': isActive('collorApril')}" @click="changeCalculator('collorApril')"><a>Collor I (04/1990)</a></li>
                        <li :class="{'is-active': isActive('collorMay')}" @click="changeCalculator('collorMay')"><a>Collor I (05/1990)</a></li>
                    </ul>
                </div>
                <div>
                    <section>
                        <h3 class="title is-4">Dados B&aacute;sicos</h3>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Nome do Correntista</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">N&uacute;mero da Conta Poupan&ccedil;a</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Dia base da Poupan&ccedil;a*</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Saldo base da Poupan&ccedil;a em {{ selectedCalculator.referenceMonth }}/{{ selectedCalculator.referenceYear }}*</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="" value="0,00">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Seguro infla&ccedil;&atilde;o (c.m.) creditado em {{ selectedCalculator.referenceMonth+1 }}/{{ selectedCalculator.referenceYear }} ($)*</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="" value="0,00">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Juros creditados em {{ selectedCalculator.referenceMonth+1 }}/{{ selectedCalculator.referenceYear }} ($)*</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="" value="0,00">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label" for="update-values-by">Atualizar valores at&eacute; (mm/aaaa)*</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="update-values-by" class="input" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                            *	Informa&ccedil;&otilde;es obrigat&oacute;rias! <br />
                            <template v-if="selectedCalculator.extraInfoMessage">
                                <br /> {{selectedCalculator.extraInfoMessage}}
                            </template>
                        </p>
                    </section>
                    <hr />

                    <section>
                        <h3 class="title is-4">Dados determinados na Senten&ccedil;a (se houver)</h3>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Data In&iacute;cio Juros Mora/Cita&ccedil;&atilde;o (mm/aaaa)</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Taxa de Juros de Mora (anual)</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <div class="select">
                                            <select>
                                                <option selected="selected">0%</option>
                                                <option>6%</option>
                                                <option>12%</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Honor&aacute;rios Advocat&iacute;cios (Percentual)</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="" value="0,00">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <hr />
                    <section>
                        <h3 class="title is-4">Dados Opcionais</h3>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label" for="data-generated-by">Dados lan&ccedil;ados por (nome)</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="data-generated-by" class="input" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Cidade (para fins de data)</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input id="" class="input" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-grouped is-grouped-centered">
                                <p class="control">
                                    <a class="button is-primary" @click="calculate">
                                        <span class="icon"><i class="fas fa-calculator"></i></span>
                                        <span>Calcular</span>
                                    </a>
                                </p>
                                <p class="control">
                                    <a class="button is-light" @click="clear">
                                        <span class="icon"><i class="fas fa-eraser"></i></span>
                                        <span>Limpar</span>
                                    </a>
                                </p>
                            </div>
                    </section>
                </div>
            </div>
            `,
        computed: {
            selectedCalculator: function() {
                return this.calculators[this.activeItem];
            }
        },
        data: function() {
            return {
                allInflationPurgeOptions: [
                    [0, '42,72% - IPC de 01/1989 (Ref. Plano Verão)'],
                    [1, '44,80% - IPC de 04/1990 (Ref. Plano Collor I)'],
                    [2, '7,87% - IPC de 05/1990 (Ref. Plano Collor I)'],
                    [3, '21,87% - IPC de 02/1991 (Ref. Plano Collor II)']
                ],
                activeItem: 'bresser',
                calculators: {
                    bresser: {
                        type: 'bresser',
                        referenceMonth: 06,
                        referenceYear: 1987,
                        inflationPurgeOptions: [0, 1, 2, 3]
                    },
                    verao: {
                        type: 'verao',
                        referenceMonth: 01,
                        referenceYear: 1989,
                        inflationPurgeOptions: [1, 2, 3],
                        extraInfoMessage: 'ATENCAO! Informe os valores acima em \'Cruzados Novos (NCz$)\'. Se for preciso, divida os valores em \'Cruzados\' por 1000.'
                    },
                    collorApril: {
                        type: 'collorApril',
                        referenceMonth: 04,
                        referenceYear: 1990,
                        inflationPurgeOptions: [2, 3]
                    },
                    collorMay: {
                        type: 'collorMay',
                        referenceMonth: 05,
                        referenceYear: 1990,
                        inflationPurgeOptions: [3]
                    }
                }
            }
        },
        methods: {
            isActive: function(calcType) {
                return this.activeItem === calcType;
            },
            changeCalculator(name) {
                this.activeItem = name;
            },
            calculate() {
                alert('Calculating...');
            },
            clear() {
                alert('Cleaning...');
            }
        }
    })

    new Vue({
        el: '#app',
    })

    /*
    {
        allInflationPurgeOptions: [
            [0, '42,72% - IPC de 01/1989 (Ref. Plano Verão)'],
            [1, '44,80% - IPC de 04/1990 (Ref. Plano Collor I)'],
            [2, '7,87% - IPC de 05/1990 (Ref. Plano Collor I)'],
            [3, '21,87% - IPC de 02/1991 (Ref. Plano Collor II)']
        ],
        bresser: {
            type: 'bresser',
            referenceMonth: 06,
            referenceYear: 1987,
            inflationPurgeOptions: [0, 1, 2, 3]
        },
        verao: {
            type: 'verao',
            referenceMonth: 01,
            referenceYear: 1989,
            inflationPurgeOptions: [1, 2, 3],
            extraInfoMessage: 'ATENÇÃO! Informe os valores acima em 'Cruzados Novos (NCz$)'. Se for preciso, divida os valores em 'Cruzados' por 1000.'
        },
        collorApril: {
            type: 'collorApril',
            referenceMonth: 04,
            referenceYear: 1990,
            inflationPurgeOptions: [2, 3]
        },
        collorMay: {
            type: 'collorMay',
            referenceMonth: 05,
            referenceYear: 1990,
            inflationPurgeOptions: [3]
        }
    } */
</script>
</body>

</html>